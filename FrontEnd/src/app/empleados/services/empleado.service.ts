import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Empleado } from '../models/empleado';
import { map } from 'rxjs/operators';
import { EmpleadosModule } from '../empleados.module';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  
  empleados: Empleado[];
  readonly URL_API = `${environment.apiUrl}/api/v1/employes`;

  constructor(private http: HttpClient) {}

  createEmploye(empleado: Empleado){
    const url = this.URL_API + "/createEmploye"
    return this.http.post(url,empleado);
  }

     isEmailValid(email:String){
    const url = this.URL_API + "/validateEmployeEmail"
    return   this.http.post(url,{"email":email}).toPromise();
  }

  getEmployeById(id: String){
    const url = this.URL_API + `/findEmploye/${id}`
    return this.http.get(url);

  }

  getEmployes(example: Empleado = Empleado.empleadoJson({}), page: number = 0, size: number = 10){
    const url = this.URL_API + "/allEmployes"
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', size.toString());
    return this.http.post(url,example,{params});
  }

  deleteEmploye(id: String){
    const url = this.URL_API + `/deleteEmploye/${id}`
    return this.http.delete(url);
  }
  
  
  
}
