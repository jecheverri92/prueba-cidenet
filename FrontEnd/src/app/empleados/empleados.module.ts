import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RegistroComponent } from './registro/registro.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { RouterModule } from '@angular/router';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [RegistroComponent, ConsultaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule,
    MatNativeDateModule,
    MatDatepickerModule,
    BrowserAnimationsModule 
  ],
  providers:[DatePipe],
  exports:[ 
    RegistroComponent,
    ConsultaComponent
  ]
})
export class EmpleadosModule { }
