import { Component, OnInit } from '@angular/core';
import { Empleado } from '../models/empleado';
import { EmpleadoService } from '../services/empleado.service';
import Swal from 'sweetalert2'
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  constructor(private empleadoService: EmpleadoService,private _fb: FormBuilder,private router: Router) { }

  empleadosList: Empleado[];

  empleadoExample: Empleado = Empleado.empleadoJson({}); 

  employeTable: FormGroup;

  page: number;
  per_page: number;
  total: number;

  ngOnInit(): void {
    this.getEmpleados(this.empleadoExample);
    this.employeTable = this._fb.group({
      primerApellido: [null],
      segundoApellido:[null],
      primerNombre: [null],
      otrosNombres: [null],
      paisEmpleo: [null],
      tipoIdentificacion: [null],
      numeroIdentificacion: [null],
      fechaIngreso: [null],
      area: [null],
      estado: [null],
      fechaRegistro: [null],
    })

    this.employeTable.valueChanges.subscribe(value => {
      this.empleadoExample = value;
      console.log(this.empleadoExample)
      this.getEmpleados(this.empleadoExample);
    });
  }


  getEmpleados(empleadoExample: Empleado) {
    this.empleadoService.getEmployes(empleadoExample)
    .subscribe( resp => {
        this.empleadosList = resp['result']?.['data'];
        this.page = resp['currentPage']+1;
        this.total= resp['totalItems'];
      });
  }

  borrarEmpleado(index: number){

    Swal.fire({
      title: 'Está seguro de que desea eliminar el empleado?',
      showDenyButton: true,
      confirmButtonText: `Si`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.empleadoService.deleteEmploye(this.empleadosList[index].id)
        .subscribe(resp=>{
          console.log(resp);
          
          this.empleadosList.splice(index,1);
        })
        Swal.fire('Borrado', '', 'success')
      } 
    
  })

   
     
  }

  pageChanged(event) {
    console.log(event);
    this.empleadoService.getEmployes(this.empleadoExample,event-1)
    .subscribe( resp => {
        console.log(resp);
        this.empleadosList = resp['result']?.['data'];
        this.page = resp['currentPage']+1;
        this.total= resp['totalItems'];
        console.log("pagina",this.page);

      });
  }

  empleadoEditar(id: number) {
    this.router.navigate(['editar', id]);
  }

}
