import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Empleado } from '../models/empleado';
import { EmpleadoService } from '../services/empleado.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  reactiveForm: FormGroup;

  empleado: Empleado = Empleado.empleadoJson({}); 

  email: String;

  emailValido: Boolean;

  dominio: String;

  currDate: string;

  editando: Boolean = false;

  idEmail: number = 0;

  constructor(private _fb: FormBuilder, 
              private empleadoService: EmpleadoService,
              private dp: DatePipe,
              private actRoute: ActivatedRoute
              ) { 
              }

  ngOnInit(): void {


    this.reactiveForm = this._fb.group({
      primerApellido: [this.empleado.primerApellido, [Validators.required, Validators.maxLength(20)]],
      segundoApellido: [this.empleado.segundoApellido, [Validators.required, Validators.maxLength(20)]],
      primerNombre: [this.empleado.primerNombre, [Validators.required, Validators.maxLength(20)]],
      otrosNombres: [this.empleado.otrosNombres, [Validators.required, Validators.maxLength(50)]],
      paisEmpleo: [this.empleado.paisEmpleo, [Validators.required]],
      tipoIdentificacion: [this.empleado.tipoIdentificacion, [Validators.required]],
      numeroIdentificacion: [this.empleado.numeroIdentificacion, [Validators.required]],
      fechaIngreso: [this.empleado.fechaIngreso, [Validators.required]],
      area: [this.empleado.area, [Validators.required]],
      estado: [this.empleado.estado, [Validators.required]],
      fechaRegistro: [this.currDate],
      fechaEdicion:[this.currDate]
    })

    

    this.editando = (this.actRoute.snapshot.routeConfig.path == "editar/:id" ) ? true : false;
    if(this.editando){
      let id = this.actRoute.snapshot.params['id'];
      this.getEmployeById(id);
    }else{
      console.log(this.currDate);
    }



    this.reactiveForm.valueChanges.subscribe(value => {
      this.empleado = value;
      this.generateEmail(value);
    });

    this.reactiveForm.controls['primerApellido'].valueChanges.subscribe(value=>{
      this.empleado = value;
      this.generateEmail(value);
    })

    this.reactiveForm.controls['primerNombre'].valueChanges.subscribe(value=>{
      this.empleado = value;
      this.generateEmail(value);
    })

    this.reactiveForm.controls['paisEmpleo'].valueChanges.subscribe(value=>{
      this.empleado = value;
      this.generateEmail(value);
    })


  }

  submitForm(){
    console.log(this.reactiveForm);
    this.registrarEmpleado(this.empleado);
  }

  async generateEmail(empleado:Empleado){
    this.dominio = (empleado.paisEmpleo == "COLOMBIA") ? "@cidenet.com.co" : "@cidenet.com.us";
    this.email = `${empleado.primerNombre}.${empleado.primerApellido}`.toLowerCase();
    let i : number = 0;
    let auxEmail = `${this.email}${this.dominio}`;
    let valido = await this.empleadoService.isEmailValid(auxEmail);
    while(valido['status']=="false"){
      auxEmail = `${this.email}.${++i}${this.dominio}`
      console.log(auxEmail);
      valido = await this.empleadoService.isEmailValid(auxEmail);
    }
    this.empleado.email = auxEmail.toLowerCase();
  }

  registrarEmpleado(empleado:Empleado){
    this.empleadoService.createEmploye(empleado)
    .subscribe(resp=>{
      console.log(resp);
      if(resp['status']=="true"){
        Swal.fire({
          icon: 'success',
          title: 'Exito',
          text: resp['message'],
        })
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: resp['message'],
        })
      }
    })
  }

  getEmployeById(id: String){
    this.empleadoService.getEmployeById(id)
    .subscribe(resp=>{
      console.log(resp['data']);
      this.empleado = resp['data'];
      this.reactiveForm.patchValue(this.empleado);

    })
  }

}
