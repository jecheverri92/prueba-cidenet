import { EmpleadoService } from '../services/empleado.service';
export class Empleado {

	static empleadoJson(obj: Object){
		return new Empleado(
			obj['id'],
			obj['primerApellido'],
			obj['segundoApellido'],
			obj['primerNombre'],
			obj['otrosNombres'],
			obj['paisEmpleo'],
			obj['tipoIdentificacion'],
			obj['numeroIdentificacion'],
			obj['email'],
			obj['fechaIngreso'],
			obj['area'],
			obj['estado'],
			obj['fechaRegistro']
		)
	}

	constructor(
		public id: String,
		public primerApellido: String,
		public segundoApellido: String,
		public primerNombre: String,
		public otrosNombres: String,
		public paisEmpleo: String,
		public tipoIdentificacion: String,
		public numeroIdentificacion: String,
		public email: String,
		public fechaIngreso: Date,
		public area: String,
		public estado: String,
		public fechaRegistro: Date
	) {

	}
    
	 
	
}