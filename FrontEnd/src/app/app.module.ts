import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//Componentes
import { AppComponent } from './app.component';
import { EmpleadosModule } from './empleados/empleados.module';
//Rutas
import { APP_ROUTING } from './app.routes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    EmpleadosModule,
    APP_ROUTING,
    NgbModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
