import { Routes, RouterModule } from '@angular/router';
import { ConsultaComponent } from './empleados/consulta/consulta.component';
import { RegistroComponent } from './empleados/registro/registro.component';


const APP_ROUTES: Routes = [
  { path: 'registro', component: RegistroComponent },
  { path: 'editar/:id', component: RegistroComponent },
  { path: '**', pathMatch: 'full', component: ConsultaComponent },
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);