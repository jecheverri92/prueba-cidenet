# Prueba Tecnica Cidenet

Desarrollo Prueba tecnica Spring-Boot + Angular

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

- Java 8+
- Maven
- node
- npm
- Angular CLI: 11.1.1

---

- Clonar el proyecto
- Descargar dependencias Angular
- Generar(generar package con maven) .jar
- Ejecutar .jar

Los siguientes comandos se deben ejecutar uno a uno:

```
git clone https://gitlab.com/jecheverri92/prueba-cidenet.git
cd prueba-cidenet/FrontEnd
npm install
cd ../BackEnd
mvn package
cd target
java -jar PruebaTecnicaCidenet-0.0.1-SNAPSHOT.jar
```

### Porbar la Aplicacion 🔧

La apliacion se ejecutara en http://localhost:6039/

### Consideraciones

- El desarrollo no se alcanzo a completar por lo tanto:
- El formulario de registro se debe llenar con MAYSUCULAS ya que no alcance a terminar las validaciones en el front.
- La funcionalidad de Editar no esta completa.
