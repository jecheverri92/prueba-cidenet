package co.com.cidenet.prueba.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import co.com.cidenet.prueba.models.Employe;

@Repository
public interface EmployeRepository extends MongoRepository<Employe, Long> {
	
	List<Employe> findByEmail(String email);
	
	Employe deleteEmployeById(String id);

	Employe findById(String id);

}
