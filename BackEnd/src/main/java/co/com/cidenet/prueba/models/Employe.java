package co.com.cidenet.prueba.models;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="empleados")
@CompoundIndex(def = "{'numeroIdentificacion' : 1, 'tipoIdentificacion' : 1}" ,unique = true)
public class Employe implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	public String id;
	
	@Pattern(regexp = "[A-Z]+")
	@Size(max = 20)
	public String primerApellido;
	
	@Pattern(regexp = "[A-Z]+")
	@Size(max = 20)
	public String segundoApellido;
	
	@Pattern(regexp = "[A-Z]+")
	@Size(max = 20)
	public String primerNombre;
	
	@Pattern(regexp = "[A-Z]+")
	@Size(max = 50)
	public String otrosNombres;
	
	public String paisEmpleo;
	
	public String tipoIdentificacion;
	
	@Size(max = 20)
	public String numeroIdentificacion;
	
	@Indexed(unique=true)
	@Email
	public String email;
	
	public Date fechaIngreso;
	
	public String area;
	
	public String estado;
	
	public Date fechaRegistro;
	
	public Date fechaEdicion;
	
	
	
	public Employe() {
		super();
	}
	
	public Employe(String id, String primerApellido, String segundoApellido, String primerNombre, String otrosNombres,
			String paisEmpleo, String tipoIdentificacion, String numeroIdentificacion, String email, Date fechaIngreso,
			String area, String estado, Date fechaRegistro, Date fechaEdicion) {
		super();
		this.id = id;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.primerNombre = primerNombre;
		this.otrosNombres = otrosNombres;
		this.paisEmpleo = paisEmpleo;
		this.tipoIdentificacion = tipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.email = email;
		this.fechaIngreso = fechaIngreso;
		this.area = area;
		this.estado = estado;
		this.fechaRegistro = fechaRegistro;
		this.fechaEdicion = fechaEdicion;
	}





	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getPrimerApellido() {
		return primerApellido;
	}



	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}



	public String getSegundoApellido() {
		return segundoApellido;
	}



	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}



	public String getPrimerNombre() {
		return primerNombre;
	}



	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}



	public String getOtrosNombres() {
		return otrosNombres;
	}



	public void setOtrosNombres(String otrosNombres) {
		this.otrosNombres = otrosNombres;
	}



	public String getPaisEmpleo() {
		return paisEmpleo;
	}



	public void setPaisEmpleo(String paisEmpleo) {
		this.paisEmpleo = paisEmpleo;
	}



	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}



	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}



	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}



	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public Date getFechaIngreso() {
		return fechaIngreso;
	}



	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}



	public String getArea() {
		return area;
	}



	public void setArea(String area) {
		this.area = area;
	}



	public String getEstado() {
		return estado;
	}



	public void setEstado(String estado) {
		this.estado = estado;
	}



	public Date getFechaRegistro() {
		return fechaRegistro;
	}



	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaEdicion() {
		return fechaEdicion;
	}

	public void setFechaEdicion(Date fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}

	
	
}
