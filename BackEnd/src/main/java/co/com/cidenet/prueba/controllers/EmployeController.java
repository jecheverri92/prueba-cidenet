package co.com.cidenet.prueba.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.cidenet.prueba.models.Employe;
import co.com.cidenet.prueba.models.Response;
import co.com.cidenet.prueba.repositories.EmployeRepository;

@RestController
@RequestMapping("/api/v1/employes")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EmployeController {
	


	@Autowired
	public EmployeRepository employeRepository;
	
	
	@PostMapping(value= "/allEmployes")
	public ResponseEntity<Map<String, Object>> getAllEmployes(
			  @RequestBody Employe employe,
		      @RequestParam(defaultValue = "0") int page,
		      @RequestParam(defaultValue = "10") int size) {
		                       
		
		ExampleMatcher matcher = ExampleMatcher.matching()
				  .withMatcher("primerNombre", match -> match.startsWith())
				  .withMatcher("otrosNombres", match -> match.startsWith())
				  .withMatcher("primerApellido", match -> match.startsWith())
				  .withMatcher("segundoApellido", match -> match.startsWith())
				  .withMatcher("tipoIdentificacion", match -> match.startsWith())
				  .withMatcher("numeroIdentificacion", match -> match.startsWith())
				  .withMatcher("paisEmpleo", match -> match.startsWith())
				  .withMatcher("estado", match -> match.startsWith());
		
		Example<Employe> example = Example.of(employe,matcher);

		
		try {
			Pageable paging = PageRequest.of(page, size);
			Page<Employe> pageEmploye;
			pageEmploye = employeRepository.findAll(example, paging);
			List<Employe> employes = pageEmploye.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("result", new Response<List<Employe>>("true", "Success", employes));
			response.put("currentPage", pageEmploye.getNumber());
		    response.put("totalItems", pageEmploye.getTotalElements());
		    response.put("totalPages", pageEmploye.getTotalPages());
			return new ResponseEntity<>(response, HttpStatus.OK);
       }
       catch (Exception e) {
    	   Map<String, Object> response = new HashMap<>();
    	   response.put("result",new Response<Employe>("false", e.getMessage(), null));
    	   return new ResponseEntity<>(response, HttpStatus.OK);
       }
		
	}
	
	@GetMapping(value= "/findEmploye/{id}")
	public Response<Employe> getEmploye(@PathVariable String id) {
		try {
			Employe employe =  employeRepository.findById(id);
			if(employe != null) {			
				return new Response<Employe>("true", "Success!" , employe);
			}
			return new Response<Employe>("false", "No existe un empleado con id " + id, null);
       }
		catch (Exception e) {
	    	   return new Response<Employe>("false", e.getMessage(), null);
	       }
	}
	
	@PostMapping(value ="/createEmploye")
	public Response<Employe> createEmploye(@Valid @RequestBody Employe employe) {
		try {
			Employe insertedEmploye = employeRepository.insert(employe);
			return new Response<Employe>("true", "Se ha creado el empleado con id: " + insertedEmploye.getId(), insertedEmploye);
       }
       catch (Exception e) {
    	   return new Response<Employe>("false", e.getMessage(), null);
       }
		
	}
	
	@DeleteMapping("/deleteEmploye/{id}")
	public Response<Boolean> deleteEmploye(@PathVariable String id) {
		
		try {
			employeRepository.deleteEmployeById(id);
			return new Response<Boolean>("true", "El usuario ha sido borrado!", true);
       }
       catch (Exception e) {
    	   return new Response<Boolean>("false", e.getMessage(), false);
       }
	}
	
	@PutMapping("/editEmploye/{id}")
	public Response<Employe> updateEmploye(@PathVariable String id, @Valid @RequestBody Employe employe) {
		
		try {
			Employe updatedEmploye = employeRepository.save(employe);
			return new Response<Employe>("true", "success!", updatedEmploye);
       }
       catch (Exception e) {
    	   return new Response<Employe>("false", e.getMessage(), null);
       }
	}
	
	@PostMapping("/validateEmployeEmail")
	public Response<Boolean> validateEmail(@RequestBody Employe employe) {	    
		try {
			List<Employe> employes = employeRepository.findByEmail(employe.getEmail());
			if(employes.size()==0){		
				return new Response<Boolean>("true", "El correo: " + employe.email + " Esta Disponible", true);
			}else {
				return new Response<Boolean>("false", "El correo: " + employe.email + " No esta Disponible", false);
			}
       }
       catch (Exception e) {
    	   return new Response<Boolean>("false", e.getMessage(), null);
       }
	}

	
	
	
	
}
